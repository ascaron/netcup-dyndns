#!/usr/bin/env python3
from requests import post, get
from os import path
from sys import exit
import logging
import configparser

logging.basicConfig(format='%(levelname)s %(asctime)s %(message)s', datefmt='%d.%m.%Y %H:%M:%S',
                    filename='ncdyndns.log', level=logging.INFO)
logger = logging.getLogger("Main")


class dyndns:

    def __init__(self):

        if not path.exists("ncdyndns.cfg"):
            config = configparser.ConfigParser()
            config['Account'] = {
                "apikey": "",
                "customernumber": "",
                "apipassword": "",
                "domainname": "",
                "debug": "False",
                "hostnames": "*,@",
            }

            with open("ncdyndns.cfg", "w") as configfile:
                config.write(configfile)

            logger.error("Config did not exist, created default")
            exit(1)
        cfg = configparser.ConfigParser()
        cfg.read("ncdyndns.cfg")

        for key in cfg["Account"]:
            if not cfg["Account"][key]:
                logger.error(key + " missing in config")
        cfg.getboolean('Account', 'debug')
        if cfg["Account"]["debug"] == "True":
            logger.setLevel(level=logging.DEBUG)

        self.apikey = cfg["Account"]['apikey']
        self.customernumber = cfg["Account"]['customernumber']
        self.apipassword = cfg["Account"]['apipassword']
        self.domainname = cfg["Account"]['domainname']
        self.hostnames = str(cfg["Account"]["hostnames"]).split(",")
        self.extip = self.getip()

    @staticmethod
    def getip():

        extip = get('https://api.ipify.org').text

        if path.isfile('oldip.txt'):
            of = open('oldip.txt', 'r')
            oldip = of.read()
            of.close()
            if extip == oldip:
                logger.info("IP did not change")
                exit(0)
            else:
                logger.info("IP changed")
                nf = open("oldip.txt", "w")
                nf.write(extip)
                nf.close()
        else:
            logger.info("Old IP file did not exist")
            nf = open("oldip.txt", "w")
            nf.write(extip)
            nf.close()
        return extip

    def login(self):
        logindata = {
            "action": "login",
            "param": {
                "apikey": self.apikey,
                "apipassword": self.apipassword,
                "customernumber": self.customernumber

            }

        }
        response = post('https://ccp.netcup.net/run/webservice/servers/endpoint.php?JSON', json=logindata).json()

        if response["status"] == "success":
            return response["responsedata"]["apisessionid"]
        else:
            logger.error(
                "DNS Record update failed, status: " + response["status"] + ', Error Message: ' + response[
                    "longmessage"])
            logger.error(response)
            exit(1)

    def getrecords(self):
        if not hasattr(self, "records"):
            action = {
                "action": "infoDnsRecords",
                "param": {
                    "domainname": self.domainname,
                    "customernumber": self.customernumber,
                    "apikey": self.apikey,
                    "apisessionid": self.login()
                }
            }

            response = post('https://ccp.netcup.net/run/webservice/servers/endpoint.php?JSON', json=action).json()
            if response['status'] == "success":
                logger.info("Retrieved dns records")
            else:
                logger.error(
                    "Getting dns records failed, status: " + response["status"] + ', Error Message: ' + response[
                        "longmessage"])
                logger.error(response)
                exit(1)

            self.records = response
        return self.records

    def getrecordid(self, hostname):

        for record in self.getrecords()["responsedata"]["dnsrecords"]:

            if record["hostname"] == hostname:
                recordid = record["id"]
        return recordid

    def updaterecord(self):
        dnsrecords = []
        for hostname in self.hostnames:
            newrecord = {
                'id': self.getrecordid(hostname),
                'hostname': hostname,
                'type': 'A',
                'priority': '0',
                'destination': self.extip,
                'deleterecord': "false",
                'state': 'yes'
            }
            dnsrecords.append(newrecord)

        action = {
            "action": "updateDnsRecords",
            "param": {
                "domainname": self.domainname,
                "customernumber": self.customernumber,
                "apikey": self.apikey,
                "apisessionid": self.login(),
                'dnsrecordset': {
                    "dnsrecords": dnsrecords
                }
            }
        }

        response = post('https://ccp.netcup.net/run/webservice/servers/endpoint.php?JSON', json=action).json()
        logger.debug(response)
        if response['status'] == "success":
            logger.info("Updated dnsrecords for " + str(self.hostnames) + " to " + self.extip)
        else:
            logger.error(
                "DNS Record update failed, status: " + response["status"] + ', Error Message: ' + response[
                    "longmessage"])
            logger.error(response)
            exit(1)


DNS = dyndns()
DNS.updaterecord()
