#!/usr/bin/env bash

apt install python python3-requests

mkdir /opt/netcup-dyndns
cp ../Main.py /opt/netcup-dyndns/Main.py
chown -R root:root /opt/netcup-dyndns
chmod 700 /opt/netcup-dyndns
chmod 700 /opt/netcup-dyndns/Main.py

cp ./netcup-dyndns.service /etc/systemd/system/netcup-dyndns.service
cp ./netcup-dyndns.timer /etc/systemd/system/netcup-dyndns.timer

chmod 755 /etc/systemd/system/netcup-dyndns.service

systemctl enable --now netcup-dyndns.timer
